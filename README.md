# Fallout Interface Extractor
A simple Action Script extractor for Fallout.

## Requirements
The game, [JPEXS Free Flash Decompiler](https://github.com/jindrapetrik/jpexs-decompiler) and [BSA_Browser](https://www.nexusmods.com/fallout4/mods/17061/?)

## How to use:

1. Clone or download the repository.

2. Place both `FFDec` and `BSA Browser Portable` into the bin folder.

3. Run `extract.bat [interface.bsa]`

Files extracted from the BSA will appear in `extracted/`.

Decompiled ActionScript will appear in `decompiled/`.

Decompiled scripts are merged into `cleaned/`. This is where you can find the complete versions of dependicies(ie `Shared`, `scaleform`, ext) as individual FLA files only compile whats used.

Since there is no documentation from decompiled code, `found/` is ment to used for finding use cases of code by scanning every AS file. You can use `utils/find.py <regex> [outfile]` to scan the decompiled code.
