@echo off

IF [%1]==[] goto NoArg
IF NOT EXIST bin\ goto NoBin

goto run

:NoArg
echo Usage: extract.bat [interface.bsa]
exit 1

:NoBin
echo \bin is not found! Please read README.md
exit 2

:run
rmdir /s /q "extracted/"
rmdir /s /q "decompiled/"

mkdir "extracted/"
mkdir "decompiled/"
mkdir "decompiled/interface/"
mkdir "decompiled/programs/"

call "bin\BSA Browser Portable\bsab.exe" /e %1 extracted/
call "bin\FFDec\ffdec.bat" -export fla decompiled/interface/ extracted/interface/
call "bin\FFDec\ffdec.bat" -export fla decompiled/programs/ extracted/programs/

:clean
rmdir /s /q "cleaned/"
mkdir "cleaned/"
mkdir "cleaned/interface/"
mkdir "cleaned/programs/"

python utils/mergeExtracted.py

find.bat