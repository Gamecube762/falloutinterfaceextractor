#!/usr/bin/env python3.6

import os
import re
import sys

if len(sys.argv) < 2:
    print('Usage: find.py <regex> [outfile]')

reg = sys.argv[1]
outfile = None if len(sys.argv) < 3 else sys.argv[2]

found = set()

for p, s, f in os.walk('cleaned'):
    for n in f:
        if n.endswith('.as'):
            with open(os.path.join(p, n), encoding='utf8') as f:
                for m in re.findall(reg ,f.read()):
                    found.add(m)

found = sorted(list(found))

if outfile:
    with open(outfile, 'w+') as f:
        f.write('\n'.join(found))

print(found)